package in.co.sdslabs.play;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Akshay on 12-03-2015.
 */
public class MasterControlsFragment extends Fragment implements View.OnClickListener{

    ImageButton play,next,vol_up , vol_down;

    private static final long CONNECTION_TIME_OUT_MS = 100;

    private GoogleApiClient client;
    private String nodeId;

    RelativeLayout rl;

    public String title , artist ;
    public Bitmap bitmap;

    TextView tv_title, tv_artist;

    public MasterControlsFragment(String title, String artist, Bitmap bitmap){
        this.bitmap = bitmap;
        this.title = title;
        this.artist = artist;
    }


    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.master_controls,container,
                false);

        rl = (RelativeLayout)view.findViewById(R.id.rl_master_fragment);
        vol_up = (ImageButton) view.findViewById(R.id.vol_down);
        vol_down = (ImageButton) view.findViewById(R.id.vol_up);
        play = (ImageButton)view.findViewById(R.id.play);
        next = (ImageButton)view.findViewById(R.id.next);
        play.setOnClickListener(this);
        next.setOnClickListener(this);
        vol_up.setOnClickListener(this);
        vol_down.setOnClickListener(this);

        play.setTag("pause");
        tv_title = (TextView) view.findViewById(R.id.title);
        tv_artist = (TextView) view.findViewById(R.id.artist);

        initApi();

        tv_title.setText(title);
        tv_artist.setText(artist);

        Handler mHandler = new Handler();
        mHandler.post(new Runnable() {
            @Override
            public void run () {
                rl.setBackground(new BitmapDrawable(getResources(), bitmap));
            }
        });

        return view;
    }

    /**
     * Initializes the GoogleApiClient and gets the Node ID of the connected device.
     */
    private void initApi(){
        client = getGoogleApiClient(getActivity());
        retrieveDeviceNode();
    }

    /**
     * Returns a GoogleApiClient that can access the Wear API.
     * @param context
     * @return A GoogleApiClient that can make calls to the Wear API
     */
    private GoogleApiClient getGoogleApiClient(Context context) {
        return new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .build();
    }

    /**
     * Connects to the GoogleApiClient and retrieves the connected device's Node ID. If there are
     * multiple connected devices, the first Node ID is returned.
     */
    private void retrieveDeviceNode() {
        new Thread(new Runnable() {
            @Override
            public void run () {
                client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                NodeApi.GetConnectedNodesResult result =
                        Wearable.NodeApi.getConnectedNodes(client).await();
                List<Node> nodes = result.getNodes();
                if (nodes.size() > 0) {
                    nodeId = nodes.get(0).getId();
                }
                client.disconnect();
            }
        }).start();
    }

    private void sendMessage(final String message) {
        if (nodeId != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    client.blockingConnect(CONNECTION_TIME_OUT_MS,
                            TimeUnit.MILLISECONDS);
                    Wearable.MessageApi.sendMessage(client, nodeId, message, null);
                    client.disconnect();
                }
            }).start();
        }
    }

    @Override
    public void onClick (View v) {
        switch(v.getId()){
            case R.id.next :
               /*Toast.makeText(getActivity(), "Next", Toast.LENGTH_SHORT).show();*/
                sendMessage("next");
                break;
            case R.id.play :
                if(play.getTag() == "pause"){
                    play.setImageBitmap(BitmapFactory.decodeResource(getResources(),
                            R.drawable.play_icon, null));
                    play.setTag("play");
                }

                else if(play.getTag() == "play"){
                    play.setImageBitmap(BitmapFactory.decodeResource(getResources(),
                            R.drawable.pause, null));
                    play.setTag("pause");
                }

               /*Toast.makeText(getActivity(),"Play",Toast.LENGTH_SHORT).show();*/
                sendMessage("play");
                break;
            case R.id.vol_up:
                sendMessage("decrease");
                break;
            case R.id.vol_down:
                sendMessage("increase");
                break;
        }
    }

}